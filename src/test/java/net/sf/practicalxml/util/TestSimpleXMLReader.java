// Copyright 2008-2014 severally by the contributors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package net.sf.practicalxml.util;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import net.sf.practicalxml.AbstractTestCase;

import org.xml.sax.InputSource;


// note: also tests XMLFilterImplBridge
public class TestSimpleXMLReader
extends AbstractTestCase
{
    public TestSimpleXMLReader(String name)
    {
        super(name);
    }


//----------------------------------------------------------------------------
//  Test Cases - we use an identity transform for testing, rather than
//               mocking out the various XMLReader handlers
//----------------------------------------------------------------------------

    public void testParseFromText() throws Exception
    {
        String src = "<root><child>text</child></root>";
        StringReader in = new StringReader(src);
        StringWriter out = new StringWriter();

        Transformer xform = TransformerFactory.newInstance().newTransformer();
        xform.transform(new SAXSource(new SimpleXMLReader(), new InputSource(in)),
                        new StreamResult(out));

        String dst = out.toString();
        assertTrue("expected: " + src + "\nwas: " + dst,
                   dst.contains(src));
    }
}
