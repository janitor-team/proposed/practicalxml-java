// Copyright 2008-2014 severally by the contributors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package net.sf.practicalxml.xpath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathFunction;
import javax.xml.xpath.XPathFunctionException;
import javax.xml.xpath.XPathFunctionResolver;


/**
 *  An <code>XPathFunctionResolver</code> that is intended to be easily configured
 *  (using the Builder pattern), can support multiple functions with the same name
 *  but different arity, and provides a simplified interface for functions that
 *  know their own name (ie, subclasses of {@link AbstractFunction}.
 *  <p>
 *  When holding functions that have the same name but different arity ranges, the
 *  resolver first looks for the smallest enclosing range, followed by the range
 *  with the smallest initial value. For example, given registrations of function
 *  "foo:bar", with arity ranges 1:1, 2:6, 2:7, and 5:10, <code>resolveFunction()</code>
 *  will act as follows:
 *  <ul>
 *  <li> given 1, returns the function registered with 1:1, because it's the only
 *       match.
 *  <li> given 2, returns the function registered with 2:6 because it coveres the
 *       smallest range. Ditto with calls for 3 to 6.
 *  <li> given 7, returns the function registered with 2:7 because it has the
 *       lowest starting value.
 *  <li> given 8, returns the function registered with 5:10 because it's the only
 *       match.
 *  </ul>
 *  <p>
 *  Note: no sane person should register functions like that.
 *  <p>
 *  Instances of this object are <em>not</em> thread-safe during construction, but
 *  are read-only (and therefore thread-safe) during usage.
 */
public class FunctionResolver
implements XPathFunctionResolver, Cloneable
{
    private Map<QName,FunctionHolder> _table = new HashMap<QName,FunctionHolder>();

//----------------------------------------------------------------------------
//  Public Methods
//----------------------------------------------------------------------------

    /**
     *  Adds an instance of of {@link #SelfDescribingFunction} to this resolver.
     *  This function provides its own information about name, namespace, and
     *  supported number of arguments.
     *  <p>
     *  If the same function has already been added, will silently replace it.
     *
     *  @return The resolver, so that calls may be chained.
     */
    public FunctionResolver addFunction(SelfDescribingFunction func)
    {
        FunctionHolder holder = _table.get(func.getQName());
        if (holder == null)
        {
            holder = new FunctionHolder(func);
            _table.put(func.getQName(), holder);
        }
        else
        {
            holder.put(func);
        }
        return this;
    }


    /**
     *  Adds a normal <code>XPathFunction</code> to this resolver, without
     *  specifying argument count. This function will be chosen for any number
     *  of arguments, provided that there is not a more-specific binding).
     *  <p>
     *  If a function has already been added with the same name and argument
     *  range, this call will silently replace it.
     *  <p>
     *  Note: the resolver wraps the passed function with a decorator that
     *  allows it to be managed. The resolver will return this decorator
     *  object rather than the original function.
     *
     *  @return The resolver, so that calls may be chained.
     */
    public FunctionResolver addFunction(XPathFunction func, QName name)
    {
        return addFunction(func, name, 0, Integer.MAX_VALUE);
    }


    /**
     *  Adds a normal <code>XPathFunction</code> to this resolver. Specifies
     *  the exact number of arguments for which this function will be chosen.
     *  <p>
     *  If a function has already been added with the same name and argument
     *  range, this call will silently replace it.
     *  <p>
     *  Note: the resolver wraps the passed function with a decorator that
     *  allows it to be managed. The resolver will return this decorator
     *  object rather than the original function.
     *
     *  @return The resolver, so that calls may be chained.
     */
    public FunctionResolver addFunction(XPathFunction func, QName name, int argCount)
    {
        return addFunction(func, name, argCount, argCount);
    }


    /**
     *  Adds a normal <code>XPathFunction</code> to this resolver. Specifies
     *  the minimum and maximum number of arguments for which this function
     *  will be chosen.
     *  <p>
     *  If a function has already been added with the same name and argument
     *  range, this call will silently replace it.
     *  <p>
     *  Note: the resolver wraps the passed function with a decorator that
     *  allows it to be managed. The resolver will return this decorator
     *  object rather than the original function.
     *
     *  @return The resolver, so that calls may be chained.
     */
    public FunctionResolver addFunction(XPathFunction func, QName name, int minArgCount, int maxArgCount)
    {
        return addFunction(new StandardFunctionAdapter(func, name, minArgCount, maxArgCount));
    }


//----------------------------------------------------------------------------
//  XPathFunctionResolver
//----------------------------------------------------------------------------

    /**
     *  Picks the "most eligble" function of those bound to this resolver.
     *  If unable to find any function that matches, returns <code>null</code>.
     */
    public XPathFunction resolveFunction(QName functionName, int arity)
    {
        FunctionHolder holder = _table.get(functionName);
        return (holder != null) ? holder.get(arity)
                                : null;
    }


//----------------------------------------------------------------------------
//  Object overrides
//----------------------------------------------------------------------------

    /**
     *  Two instances are equal if they have the same number of functions,
     *  and each has a counterpart with the same QName and arity range (but
     *  not the same implementation).
     */
    @Override
    public final boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        else if (obj instanceof FunctionResolver)
            return this._table.equals(((FunctionResolver)obj)._table);
        else
            return false;
    }


    @Override
    public final int hashCode()
    {
        // I suspect this is a very inefficient way to compute hashcode,
        // but this object should never be stored in a hashing structure
        // -- if you need to do that, come up with a better implementation
        return _table.keySet().hashCode();
    }


    /**
     *  Returns a string that lists this resolver's functions, intended for
     *  debugging. The output is similar to that of <code>List.toString()</code>,
     *  with each item having the form <code>{NS}NAME(MIN:MAX)</code>, where:
     *  <ul>
     *  <li> <code>NS</code> is the function's namespace URI
     *  <li> <code>NAME</code> is the function's localname
     *  <li> <code>MIN</code> is the minimum arity of the function
     *  <li> <code>MAX</code> is the maximum arity of the function
     *  </ul>
     *
     *  @since 1.1.12
     */
    @Override
    public String toString()
    {
        List<String> items = new ArrayList<String>();
        for (Map.Entry<QName,FunctionHolder> fn : _table.entrySet())
        {
            QName name = fn.getKey();
            FunctionHolder holder = fn.getValue();
            StringBuilder sb = new StringBuilder(64);
            for (SelfDescribingFunction func : holder.getAll())
            {
                sb.append("{").append(name.getNamespaceURI()).append("}")
                  .append(name.getLocalPart())
                  .append("(").append(func.getMinArgCount())
                  .append(":").append(func.getMaxArgCount())
                  .append(")");
                items.add(sb.toString());
            }
        }
        return items.toString();
    }


    /**
     *  Returns a shallow clone of this object, containing all existing
     *  functions in a new lookup table. Does not attempt to clone the
     *  functions themselves; if you create functions that have mutable
     *  state, you need to explicitly copy them.
     *
     *  @since 1.1.12
     */
    @Override
    public FunctionResolver clone()
    {
        FunctionResolver ret;
        try
        {
            ret = (FunctionResolver)super.clone();
            ret._table = new HashMap<QName,FunctionHolder>(_table);
            return ret;
        }
        catch (CloneNotSupportedException e)
        {
            throw new RuntimeException(
                    "should never be thrown unless class hierarchy is changed", e);
        }
    }


//----------------------------------------------------------------------------
//  Internals
//----------------------------------------------------------------------------

    /**
     *  A self-describing function: one that can describe its own name and
     *  arity, and compare itself to other self-describing functions.
     *  <p>
     *  Implementations must implement <code>Comparable</code> to allow the
     *  function resolver to pick the most appropriate instance for a specific
     *  invocation. The primary issue is with arity, so any comparator must
     *  follow these rules:
     *  <ul>
     *  <li> Instances that accept fewer arguments are considered less-than
     *       instances that accept more.
     *  <li> If two instances accept the same number of arguments, the instance
     *       with the lower minimum argument count is less-than that with the
     *       higher count.
     *  </ul>
     */
    public interface SelfDescribingFunction
    extends XPathFunction, Comparable<SelfDescribingFunction>
    {
        /**
         *  Returns the qualified name of this function, consisting of name and
         *  namespace (but not prefix).
         */
        public QName getQName();

        /**
         *  Returns the namespace URI for this function.
         */
        public String getNamespaceUri();

        /**
         *  Returns the name of this function.
         */
        public String getName();

        /**
         *  Returns the minimum number of arguments handled by this function.
         */
        public int getMinArgCount();

        /**
         *  Returns the maximum number of arguments handled by this function.
         */
        public int getMaxArgCount();

        /**
         *  Determines whether this function is a match for a call to
         *  <code>XPathFunctionResolver.resolveFunction()</code>.
         */
        public boolean isMatch(QName qname, int arity);

        /**
         *  Determines whether this function can handle the specified number of
         *  arguments.
         */
        public boolean isArityMatch(int arity);
    }


    /**
     *  A base implementation of a self-describing function.
     */
    public abstract static class AbstractSelfDescribingFunction
    implements SelfDescribingFunction
    {
        final private QName _qname;
        final private int _minArgCount;
        final private int _maxArgCount;

        protected AbstractSelfDescribingFunction(QName qname, int minArgs, int maxArgs)
        {
            _qname = qname;
            _minArgCount = minArgs;
            _maxArgCount = maxArgs;
        }

        public QName getQName()
        {
            return _qname;
        }

        public String getNamespaceUri()
        {
            return _qname.getNamespaceURI();
        }

        public String getName()
        {
            return _qname.getLocalPart();
        }

        public int getMinArgCount()
        {
            return _minArgCount;
        }

        public int getMaxArgCount()
        {
            return _maxArgCount;
        }

        public boolean isMatch(QName qname, int arity)
        {
            return _qname.equals(qname)
                && (arity >= _minArgCount)
                && (arity <= _maxArgCount);
        }

        public boolean isArityMatch(int arity)
        {
            return (arity >= _minArgCount)
                && (arity <= _maxArgCount);
        }

        public int compareTo(SelfDescribingFunction that)
        {
            int result = this.getNamespaceUri().compareTo(that.getNamespaceUri());
            if (result != 0)
                return (result < 0) ? -1 : 1;

            result = this.getName().compareTo(that.getName());
            if (result != 0)
                return (result < 0) ? -1 : 1;

            result = (this.getMaxArgCount() - this.getMinArgCount()) - (that.getMaxArgCount() - that.getMinArgCount());
            if (result != 0)
                return (result < 0) ? -1 : 1;

            result = this.getMinArgCount() - that.getMinArgCount();
            if (result != 0)
                return (result < 0) ? -1 : 1;

            return result;
        }

        @Override
        public final boolean equals(Object obj)
        {
            if (this == obj)
                return true;
            else if (obj instanceof AbstractSelfDescribingFunction)
            {
                AbstractSelfDescribingFunction that = (AbstractSelfDescribingFunction)obj;
                return this._qname.equals(that._qname)
                    && this._minArgCount == that._minArgCount
                    && this._maxArgCount == that._maxArgCount;
            }
            else
                return false;
        }


        @Override
        public final int hashCode()
        {
            return _qname.hashCode();
        }
    }


    /**
     *  Holder for function instances associated with the same qname, which will
     *  pick the appropriate function for a given requested arity.
     */
    private static class FunctionHolder
    {
        private SelfDescribingFunction _onlyOne;
        private TreeSet<SelfDescribingFunction> _hasMany;

        public FunctionHolder(SelfDescribingFunction initial)
        {
            _onlyOne = initial;
        }

        public void put(SelfDescribingFunction func)
        {
            if (_hasMany != null)
            {
                // remove old implementation if it exists
                _hasMany.remove(func);
                _hasMany.add(func);
            }
            else if (_onlyOne.equals(func))
            {
                _onlyOne = func;
            }
            else
            {
                _hasMany = new TreeSet<SelfDescribingFunction>();
                _hasMany.add(func);
                _hasMany.add(_onlyOne);
                _onlyOne = null;
            }
        }

        public SelfDescribingFunction get(int arity)
        {
            if (_onlyOne != null)
            {
                return (_onlyOne.isArityMatch(arity))
                       ? _onlyOne
                       : null;
            }

            for (SelfDescribingFunction func : _hasMany)
            {
                if (func.isArityMatch(arity))
                    return func;
            }

            return null;
        }

        public Set<SelfDescribingFunction> getAll()
        {
            if (_onlyOne != null)
                return new TreeSet<SelfDescribingFunction>(Arrays.asList(_onlyOne));
            else
                return _hasMany;
        }

        @Override
        public boolean equals(Object obj)
        {
            // if this ever fails, someone needs to fix their code ... but
            // FindBugs complained, and I didn't feel comfortable filtering
            if (obj instanceof FunctionHolder)
            {
                FunctionHolder that = (FunctionHolder)obj;

                return (_onlyOne != null)
                     ? this._onlyOne.equals(that._onlyOne)
                     : this._hasMany.equals(that._hasMany);
            }
            return false;
        }

        @Override
        public int hashCode()
        {
            // I know this will never be stored as the key of a hash-table,
            // but I want to keep static analysis tools quiet
            return 0;
        }
    }


    /**
     *  Adapter for standard XPathFunction instances, allowing them to be
     *  managed by this resolver.
     */
    private static class StandardFunctionAdapter
    extends AbstractSelfDescribingFunction
    {
        final XPathFunction _func;

        public StandardFunctionAdapter(XPathFunction func, QName name, int minArgCount, int maxArgCount)
        {
            super(name, minArgCount, maxArgCount);
            _func = func;
        }

        @SuppressWarnings("rawtypes")
        public Object evaluate(List args)
        throws XPathFunctionException
        {
            return _func.evaluate(args);
        }
    }
}
