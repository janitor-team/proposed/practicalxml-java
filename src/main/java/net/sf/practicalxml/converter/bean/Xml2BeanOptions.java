// Copyright 2008-2014 severally by the contributors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package net.sf.practicalxml.converter.bean;


/**
 *  Options used by {@link Xml2BeanConverter} to control the way that DOM trees
 *  are translated to Java beans.
 *
 *  @since 1.1
 */
public enum Xml2BeanOptions
{
    /**
     *  Expects byte arrays as a Base64-encoded string
     *
     *  @since 1.1.18
     */
    BYTE_ARRAYS_AS_BASE64,

    /**
     *  Outputs byte arrays as a hex-encoded string
     *
     *  @since 1.1.18
     */
    BYTE_ARRAYS_AS_HEX,

    /**
     *  Will use a shared static introspection cache for all conversions.
     *  <p>
     *  <strong>Warning</strong>: if you use this option, do not store this
     *  library in a shared app-server classpath. If you do, the cache will
     *  prevent class unloading, and you will run out of permgen space.
     */
    CACHE_INTROSPECTIONS,

    /**
     *  Will attempt to convert attribute values as well as elements. Will not
     *  convert attributes with XML Schema Instance namespace or the converter's
     *  own namespace, but otherwise ignores namespace.
     *  <p>
     *  Child elements will be converted after attributes; if an element has
     *  the same name as an attribute, the element value takes precedence.
     */
    CONVERT_ATTRIBUTES,

    /**
     *  Will attempt to convert attribute values as well as elements, ignoring
     *  all attributes that do not have the same namespace as the element they
     *  belong to (includes null namespaces).
     *  <p>
     *  Child elements will be converted after attributes; if an element has
     *  the same name as an attribute, the element value takes precedence.
     */
    CONVERT_ATTRIBUTES_MATCH_NAMESPACE,

    /**
     *  Defer exception processing. When an exception occurs during conversion,
     *  the node causing that exception is skipped and the exception is added
     *  to a list. The caller can then examine that list and take appropriate
     *  action.
     *  <p>
     *  Note: all deferred exceptions will be of type
     *  {@link net.sf.practicalxml.converter.ConversionException}, and will wrap
     *  an underlying cause. Because the converter works by reflection, in many
     *  cases this cause will be an <code>InvocationTargetException</code> with
     *  its own cause.
     *
     *  @since 1.1.15
     */
    DEFER_EXCEPTIONS,

    /**
     *  If present, the converter will treat all elements with empty text nodes
     *  as if they were empty elements -- in other words, <code>null</code>.
     *  Note that this flag will interact with <code>REQUIRE_XSI_NIL</code>.
     */
    EMPTY_IS_NULL,

    /**
     *  Match enums by their <code>toString()</code> values. By default, enums are
     *  matched by name (which is also the default string value).
     *
     *  @since 1.1.17
     */
    ENUM_AS_STRING_VALUE,

    /**
     *  Expect data (in particular, dates and booleans) to be formatted per XML Schema
     *  specifications.
     */
    EXPECT_XSD_FORMAT,

    /**
     *  If present, the converter ignores elements that don't correspond to
     *  settable properties of the bean.
     */
    IGNORE_MISSING_PROPERTIES,

    /**
     *  If present, the converter requires a <code>type</code> attribute on
     *  each element, and will use that attribute to verify that the element
     *  can be converted to the desired type.
     */
    REQUIRE_TYPE,

    /**
     *  If present, the converter requires an <code>xsi:nil</code> attribute
     *  on any empty nodes, and will throw if it's not present. Default is to
     *  treat empty nodes as <code>null</code>.
     */
    REQUIRE_XSI_NIL
}
